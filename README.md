__HMC (Hyperbola Multimedia Collections), formerly:__
=====================================================
*   __PMC (Parabola Multimedia Collections)__
*   __PAC (Parabola Art Collections)__
*   __POM (Parabola Official Multimedia)__

---

It's _deprecated_ collections of<br/>
multimedia files for __Hyperbola Project__.

---

The __COPYING__ file in this _directory_ contains<br/>
_attributions_ and _licensing terms_ for this collection.
